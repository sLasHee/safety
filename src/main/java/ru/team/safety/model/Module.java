package ru.team.safety.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.sql.Timestamp;

@Getter
@Setter
public class Module {

    @Id
    private Long id;
    private String name;
    private String config;
    private Long userId;
    @Schema(example = "false")
    private Boolean security;
    @Schema(example = "false")
    private Boolean alert;
    @CreatedDate
    private Timestamp createdTimestamp;
    @LastModifiedDate
    private Timestamp updatedTimestamp;
}
