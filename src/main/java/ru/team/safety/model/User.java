package ru.team.safety.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.sql.Timestamp;

@Getter
@Setter
public class User {

    @Id
    private Long id;
    private String fullName;
    private String login;
    private String password;
    private String email;
    @Schema(example = "false")
    private Boolean subscription;
    @CreatedDate
    private Timestamp createdTimestamp;
    @LastModifiedDate
    private Timestamp updatedTimestamp;
}
