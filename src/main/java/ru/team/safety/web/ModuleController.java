package ru.team.safety.web;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.team.safety.model.Module;
import ru.team.safety.service.ModuleService;
import ru.team.safety.web.dto.ModuleDTO;

import java.util.List;

@RestController
@RequestMapping("api/module")
@Tag(name = "Operations on the Module")
public class ModuleController {
    @Autowired
    private ModuleService moduleService;

    @Operation(summary = "Create Module")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Module has been created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Module.class))})})
    @PostMapping
    public Module create(@RequestBody ModuleDTO moduleDTO) {
        return moduleService.create(moduleDTO);
    }

    @Operation(summary = "Find module")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Module found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Module.class))})})
    @GetMapping("/{id}")
    public Module getModule(@PathVariable(name = "id") Long id) {
        return moduleService.getModule(id);
    }

    @Operation(summary = "Find all modules")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Modules found successfully",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Module.class)))})})
    @GetMapping
    public List<Module> getModules() {
        return moduleService.getModules();
    }


    @Operation(summary = "Find modules by user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Found modules successfully",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Module.class)))})})
    @GetMapping("/getModulesByUserId/{userId}")
    public List<Module> getModulesByUserId(@PathVariable(name = "userId") Long groupId) {
        return moduleService.getModulesByUserId(groupId);
    }

    // TODO: 18.02.2023 required add another responses with status code for all endpoints
}
