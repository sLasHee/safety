package ru.team.safety.web;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.team.safety.model.User;
import ru.team.safety.service.EmailSender;
import ru.team.safety.service.UserService;
import ru.team.safety.web.dto.UserDTO;


@RestController
@RequestMapping("api/user")
@Tag(name = "Operations on the User")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private EmailSender emailSender;

    @Operation(summary = "Create user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User has been created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class))})})
    @PostMapping
    public User create(@RequestBody UserDTO userDTO) {
        User user = userService.create(userDTO);
        emailSender.send(userDTO.getEmail());
        return user;
    }

    // TODO: 18.02.2023 required add another responses with status code for all endpoints
}
