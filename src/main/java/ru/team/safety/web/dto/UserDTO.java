package ru.team.safety.web.dto;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.Data;

@Data
public class UserDTO {
    private String fullName;
    private String login;
    private String password;
    private String email;
    @Hidden
    private Boolean subscription;
}
