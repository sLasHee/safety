package ru.team.safety.web.dto;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.Data;

@Data
public class ModuleDTO {

    private String name;
    private String config;
    private Long userId;
    @Hidden
    private boolean security;
    @Hidden
    private boolean alert;
}
