package ru.team.safety.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.team.safety.model.Module;
import ru.team.safety.repository.ModuleRepository;
import ru.team.safety.web.dto.ModuleDTO;

import java.util.ArrayList;
import java.util.List;

@Service
public class ModuleService {

    @Autowired
    private ModuleRepository repository;

    public Module create(ModuleDTO moduleDTO) {
        Module module = new Module();
        module.setName(moduleDTO.getName());
        module.setConfig(moduleDTO.getConfig());
        module.setUserId(moduleDTO.getUserId());
        module.setSecurity(false);
        module.setAlert(false);
        return repository.save(module);
    }

    public Module getModule(Long id) {
        return repository.getModuleById(id);
    }

    public List<Module> getModules() {
        List<Module> modules = new ArrayList<>();
        Iterable<Module> iterable = repository.findAll();
        iterable.forEach(modules::add);
        return modules;
    }

    public List<Module> getModulesByUserId(Long groupId) {
        return repository.getModulesByUserId(groupId);
    }

}
