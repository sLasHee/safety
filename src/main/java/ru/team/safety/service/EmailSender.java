package ru.team.safety.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;

import static javax.mail.Message.RecipientType.TO;


@Service
@Slf4j
public class EmailSender {

    private final static String FROM = "cryptonotification@bk.ru";
    private final static String SUBJECT = "Регистрация в приложении";
    private final static String TEXT = "Для подтверждения ваших учетных данных используйте код: ";

    @Autowired
    private JavaMailSender mailSender;

    public void send(String to) {
        try {
            mailSender.send(message -> {
                message.setFrom(new InternetAddress(FROM));
                message.setRecipients(TO, to);
                message.setSubject(SUBJECT, "UTF-8");
                message.setText(getTextWithCode());
            });
        } catch (Exception e) {
            log.error("MessagingException appeared while sending email to {}", to, e);
            throw e;
        }
    }

    private String getTextWithCode() {
        int code = (int) Math.abs(Math.random() * 10000);
        return TEXT.concat(String.valueOf(code));
    }
}
