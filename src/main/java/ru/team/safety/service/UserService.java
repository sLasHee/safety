package ru.team.safety.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.team.safety.model.User;
import ru.team.safety.repository.UserRepository;
import ru.team.safety.web.dto.UserDTO;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public User create(UserDTO userDTO) {
        User user = new User();
        user.setFullName(userDTO.getFullName());
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setSubscription(false);
        return repository.save(user);
    }
}
