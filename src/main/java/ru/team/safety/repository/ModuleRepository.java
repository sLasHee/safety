package ru.team.safety.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.team.safety.model.Module;

import java.util.List;

@Repository
public interface ModuleRepository extends CrudRepository<Module, Long> {

    Module getModuleById(Long id);
    List<Module> getModulesByUserId(Long userId);
}
