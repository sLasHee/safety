package ru.team.safety.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.team.safety.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
